import requests
from bs4 import BeautifulSoup
import csv
import time
import unittest

request = requests.get("https://sd42.nl/curriculum")
soup = BeautifulSoup(request.text, 'html.parser')
modules = soup.find_all('h2')

module_list = []

for module in modules:
    module_list.append(module.get_text())

print(module_list)


timestamp = f"{time.time()}.csv"
with open(timestamp, 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(module_list)


class Testing(unittest.TestCase):
    def test_count(self):
        expected = 20
        module_count = len(module_list)
        self.assertEqual(expected, module_count)

if __name__ == '__main__':
    unittest.main()
